.. image:: https://img.shields.io/pypi/v/tempora.svg
   :target: https://pypi.org/project/tempora

.. image:: https://img.shields.io/pypi/pyversions/tempora.svg

.. image:: https://dev.azure.com/jaraco/tempora/_apis/build/status/jaraco.tempora?branchName=master
   :target: https://dev.azure.com/jaraco/tempora/_build/latest?definitionId=1&branchName=master

.. image:: https://img.shields.io/travis/jaraco/tempora/master.svg
   :target: https://travis-ci.org/jaraco/tempora

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style: Black

.. .. image:: https://img.shields.io/appveyor/ci/jaraco/tempora/master.svg
..    :target: https://ci.appveyor.com/project/jaraco/tempora/branch/master

.. image:: https://readthedocs.org/projects/tempora/badge/?version=latest
   :target: https://tempora.readthedocs.io/en/latest/?badge=latest

Objects and routines pertaining to date and time (tempora).

Modules include:

- tempora (top level package module) contains miscellaneous
   utilities and constants.
- timing contains routines for measuring and profiling.
- schedule contains an event scheduler.
- utc contains routines for getting datetime-aware UTC values
  (Python 3 only).
